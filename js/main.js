// Show button on scroll
window.onscroll = function() { toggle_gototop() };

function toggle_gototop() {
  gototop_button = document.getElementById("gototop");
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    gototop_button.style.display = "block";
  } else {
    gototop_button.style.display = "none";
  }
}

document.getElementById("gototop").addEventListener("click", function(event) {
  document.body.scrollTop = 0; // For Safari
  document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
});

document.addEventListener('DOMContentLoaded', function() {
  const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);
  if ($navbarBurgers.length > 0) {
    $navbarBurgers.forEach(function(el) {
      el.addEventListener('click', function() {
        const target = el.dataset.target;
        const $target = document.getElementById(target);
        el.classList.toggle('is-active');
        $target.classList.toggle('is-active');
      });
    });
  }
});

function flex_pcb_calc() {
  const phone_length = parseFloat(document.getElementById("phone_length").value);
  const phone_width = parseFloat(document.getElementById("phone_width").value);
  document.getElementById("flex_pcb_length").value = phone_length + phone_width;
}

/*
document.getElementById("flex_pcb_calc").onsubmit = function(e) {
  const phone_length = document.getElementById("phone_length").value
  const phone_width = document.getElementById("phone_width").value;

  document.getElementById("flex_pcb_length").value = phone_length + phone_width;
  e.preventDefault();
};
*/

/*
const flex_pcb_calc = document.getElementById("flex_pcb_calc");
if (flex_pcb_calc.addEventListener) {
  flex_pcb_calc.addEventListener("submit", function(event) {
    event.preventDefault();
    window.history.back();
  }, true);
} else {
  flex_pcb_calc.attachEvent('onsubmit', function(event){
    event.preventDefault();
    window.history.back();
  });
}
*/

