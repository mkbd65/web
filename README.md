![logo][logo]

[![pipeline status](https://gitlab.com/mkbd65/web/badges/main/pipeline.svg)](https://gitlab.com/mkbd65/web/-/commits/main)

# mkbd65 static web site

Small static page describing the project

See it in action [here](https://mkbd65.xengi.de)!

## keyboard layout editor

### ANSI

[![ANSI layout][ansi_img]][ansi_kle]

### ISO

[![ISO layout][iso_img]][iso_kle]

---

![CC BY-SA 4.0](https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/by-sa.svg)
<p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://gitlab.com/mkbd65/case">mkbd65 case</a> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://mkbd65.xengi.de">XenGi</a> is licensed under <a href="http://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-SA 4.0</a></p>


[logo]: https://gitlab.com/mkbd65/web/-/raw/main/img/logo.png
[ansi_kle]: http://www.keyboard-layout-editor.com/#/gists/7dd07f537fdabe4e8c25ad3b9b0d8173
[iso_kle]: http://www.keyboard-layout-editor.com/#/gists/ea8b9739acc4a8de44a7013d14665932
[ansi_img]: https://gitlab.com/mkbd65/web/-/raw/main/mkbd65-ansi.png
[iso_img]: https://gitlab.com/mkbd65/web/-/raw/main/mkbd65-iso.png
