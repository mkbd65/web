.PHONY: build clean setup serve

build: clean
	mkdir -p ./public/{css,js,img,fonts}
	cp ./node_modules/bulma/css/bulma.min.css ./public/css/
	cp ./node_modules/fork-awesome/css/fork-awesome.min.css ./public/css/
	cp ./node_modules/fork-awesome/css/fork-awesome.min.css.map ./public/css/
	cp ./node_modules/fork-awesome/fonts/forkawesome-webfont.* ./public/fonts/
	cp ./mkbd65-{ansi,iso}.png ./mkbd65-{ansi,iso}-fn.png ./public/img/
	cp ./img/mkbd65_{74x74,148x148,222x222}.png ./public/img/
	cp -r ./css ./public/
	cp -r ./js ./public/
	cat ./{header,index,footer}.html > public/index.html
	cat ./{header,build,footer}.html > public/build.html

clean:
	rm -rf ./public

setup:
	yarn

serve:
	python -m http.server -d ./public

