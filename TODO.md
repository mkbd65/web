TODO
====

Update docs
-----------

- minimum requirements for phones
  - minimum width (long side) 140mm (130mm PCB plus 10mm case)
  - minimum length (short side) 70mm (65mm PCB plus 5mm case)
    - _TODO: tyical phone size is 75mm but zenphones can be 68mm; maybe add daughter board or flex cable for charge through USB port?_

Add build guide
---------------

1. Buy electronic parts
2. Buy PCB and flex cable
3. Configure QMK firmware
4. Flash firmware
5. Test
5. Print case parts
6. Assemble
7. Fun!


Mod ideas for community repos
-----------------------------

- 3D files and QMK config for additional layouts like neo
- 3D files for case mods
- multi key mod
  - very big keys that cover multiple switches could have multiple functions depending on where you press them
- space bar mod
  - A long spacebar could integrate a metal (hard material) pice in the key cap and only press the two switches on it's side. Then you only solder in the outer switches und use the metal bar in your cap to press only one of them.
